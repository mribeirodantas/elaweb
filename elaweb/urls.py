# -*- encoding: utf-8 -*-
from django.conf.urls import patterns, include, url
from supervisorio.views import hello, error404, statistics
from supervisorio.views import monitorar, get_pname, spo2, fr, fc
from supervisorio.views import fa, vt, allparams, add_patient
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'elaweb.views.home', name='home'),
    # url(r'^elaweb/', include('elaweb.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/$', include(admin.site.urls)),
    url(r'^$', login_required(hello)),
    url(r'^monitorar/$', login_required(monitorar)),
    url(r'^monitorar/(.*)/$', TemplateView.as_view(template_name='supervisorio/error.html'),
        {'mensagem' : ' Você precisa informar os dados do paciente' }),
    url(r'^api/get_pname/', login_required(get_pname), name='get_pname'),
    url(r'^monitorar/spo2/(.*):(.*):(.*)-(.*)', login_required(spo2)),
    url(r'^monitorar/fr/(.*):(.*)', login_required(fr)),
    url(r'^monitorar/fc/(.*):(.*)', login_required(fc)),
    url(r'^monitorar/fa/(.*):(.*)', login_required(fa)),
    url(r'^monitorar/vt/(.*):(.*)', login_required(vt)),
    url(r'^monitorar/all/(.*):(.*)', login_required(allparams)),
    url(r'^statistics/$', login_required(statistics)),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'supervisorio/login.html'}),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'paciente/add/$', add_patient),
    url(r'^(.*)', login_required(error404)),
    #That's for specific URLs to the supervisorio app
    #url(r'^supervisorio/', include('supervisorio.urls')),
)
