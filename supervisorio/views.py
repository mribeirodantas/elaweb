# -*- coding: utf-8 -*-
# Create your views here.
from django.http import HttpResponse,HttpRequest,HttpResponseRedirect
from supervisorio.models import Paciente, Aparelho, Monitoramento
from django.contrib.auth.models import User, Group
from django.utils import timezone
#from django.template.loader import get_template
#from django.template import Context
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import time
import json
from django.core.validators import RegexValidator, MinLengthValidator
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from supervisorio.forms import PacienteForm
import pytz

def hello(request):
    #return HttpResponse("ELA is coming soon!")
    #hello = "Hello World"
    #html = u"<html><body>%s</body></html>" % hello
    #return HttpResponse(html)
    return render(request, 'supervisorio/main.html', {})

def monitorar(request):
    return render(request, 'supervisorio/monitorar.html', {})

def get_pname(request):
     if request.is_ajax():
         q = request.GET.get('term', '')
         pname = Paciente.objects.filter(nomePaciente__icontains = q )[:20]
         results = []
         for name in pname:
             name_json = {}
             label = unicode(name.nomePaciente) + " ID:" + unicode(name.idPaciente) + " CPF:" + unicode(name.cpfPaciente)
             value = unicode(name.nomePaciente) + ":" + unicode(name.idPaciente)
             name_json['label'] = label
             name_json['value'] = value
             results.append(name_json)
         data = json.dumps(results)
     else:
         data = 'fail'
     mimetype = 'application/json'
     return HttpResponse(data, mimetype)


def error404(request, word):
    mensagem =  " Procurando por \"%s\" ? Essa pagina nao existe" % word
    return render(request, 'supervisorio/error.html', {
        'mensagem' : mensagem})

def statistics(request):
    i = 0
    for p in Paciente.objects.all():
        i += 1
    num_paciente = i
    i = 0
    for a in Aparelho.objects.all():
        i += 1
    num_aparelho = i
    i = 0
    for m in Monitoramento.objects.all():
        i += 1
    num_monitoramento = i
    i = 0

    for u in User.objects.all():
        i += 1
    num_user = i
    i = 0
    for g in Group.objects.all():
        i += 1
    num_group = i
    #t = get_template('supervisorio/statistics.html')
    #html = t.render(Context({'now' : str(timezone.now()), 'numPacientes' : num_paciente, 'numAparelhos' : num_aparelho,
    #    'numMonitoramentos' : num_monitoramento, 'numUsuarios' : num_user, 'numGrupos' : num_group}))
    #return HttpResponse(html)
    return render(request, 'supervisorio/statistics.html', {'now' : str(timezone.now()),
        'numPacientes' : num_paciente, 'numAparelhos' : num_aparelho, 'numMonitoramentos' : num_monitoramento,
        'numUsuarios' : num_user, 'numGrupos' : num_group})

# Parameters to be passed to template
param = []

def check_pname_id(nomepaciente, idpaciente):
    # Generates a list of patients with the name nomepaciente
    # (There maybe more than one person with the same name)
    # Test if the id passed corresponds to any of them
    for paciente in Paciente.objects.filter(nomePaciente=nomepaciente):
        if paciente.idPaciente == int(idpaciente):
            return True

def is_within_the_range(data_inicial, data_final, data):
    tz = pytz.timezone('America/Recife')
    data = data.astimezone(tz)
    # Checking year consistency
    if (data.date().year > int(data_final[6:10])) or (data.date().year < int(data_inicial[6:10])):
        return False
    # Checking month consistency
    elif (data.date().month > int(data_final[3:5])) or (data.date().month < int(data_inicial[3:5])):
        return False
    # Checking day consistency
    elif (data.date().day > int(data_final[0:2])) or (data.date().day < int(data_inicial[0:2])):
        return False
    # It is within the range specified.
    else:
        return True


def spo2(request, nomepaciente, idpaciente, data_inicial, data_final):
    # idPaciente e nomePaciente não são valores que existem no banco?
    if not Paciente.objects.filter(nomePaciente=nomepaciente) and not Paciente.objects.filter(idPaciente=idpaciente):
        mensagem =  " Este paciente não existe"
        return render(request, 'supervisorio/error.html', {
            'mensagem' : mensagem})
    # Se ambos são valores existentes no banco..
    else:
        # Correspondem a mesma pessoa?
        if (check_pname_id(nomepaciente, idpaciente) == True):
            # Se correspondem a mesma pessoa, tem algum monitoramento?
            monitoramento = Monitoramento.objects.filter()
            if [ each.SpO2 for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]:
                # Setting correct Timezone
                tz = pytz.timezone('America/Recife')

                # builds both spo2 x and y axis
                # It converts dates and times to correct timezone
                param = ["[Date.UTC(" + str(each.timestamp.astimezone(tz).date().year) + "," + str(each.timestamp.astimezone(tz).date().month - 1) + "," + str(each.timestamp.astimezone(tz).date().day) + "," + str(each.timestamp.astimezone(tz).time().hour) + "," + str(each.timestamp.astimezone(tz).time().minute) + "," + str(each.timestamp.astimezone(tz).time().second) + "), " + str(each.SpO2) + "]" for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) and is_within_the_range(data_inicial, data_final, each.timestamp) ]
                # Join every element of the 'param' string list into a single string with param's elements separated by ','
                params = ','.join(param)

                timestamps = [ each.timestamp for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) and 
                               is_within_the_range(data_inicial, data_final, each.timestamp) ]
                if len(timestamps) < 1:
                    mensagem =  " Não houve monitoramento neste período"
                    return render(request, 'supervisorio/error.html', {
                        'mensagem' : mensagem})

                return render(request, 'supervisorio/spo2.html', {
                    'nomePaciente' : nomepaciente,
                    'id' : idpaciente,
                    'pontos' : params})
            # Então não tem nenhum monitoramento?
            else:
                mensagem =  " Este paciente ainda não foi monitorado"
                return render(request, 'supervisorio/error.html', {
                    'mensagem' : mensagem})
        else:
            # O id e o nome não correspondem ao mesmo paciente
            mensagem =  " O ID " + idpaciente + " nao corresponde ao (a) paciente " + nomepaciente
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})

def fr(request, nomepaciente, idpaciente):
    # idPaciente e nomePaciente não são valores que existem no banco?
    if not Paciente.objects.filter(nomePaciente=nomepaciente) and not Paciente.objects.filter(idPaciente=idpaciente):
        mensagem =  " Este paciente não existe"
        return render(request, 'supervisorio/error.html', {
            'mensagem' : mensagem})
    # Se ambos são valores existentes no banco..
    else:
        # Correspondem a mesma pessoa?
        if (check_pname_id(nomepaciente, idpaciente) == True):
            # Se correspondem a mesma pessoa, tem algum monitoramento?
            monitoramento = Monitoramento.objects.filter()
            if [ each.FRtotal for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]:
                # builds spo2 (y-axis) list
                param = [ each.FRtotal for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]
                # builds timestamp list
                #timestamp = []
                #for each in monitoramento:
                    #better later fix comparing id instead of names
                    #if (each.idPaciente.nomePaciente == nomepaciente):
                        #hour = str(each.timestamp.time().hour)
                        #minute = str(each.timestamp.time().minute)
                        #second = str(each.timestamp.time().second)
                        #time = hour + ":" + minute + ":" + second
                        #timestamp.append(time)

                return render(request, 'supervisorio/fr.html', {
                    'nomePaciente' : nomepaciente,
                    #'timestamp' : timestamp,
                    'id' : idpaciente,
                    'fr' : param})
            # Então não tem nenhum monitoramento?
            else:
                mensagem =  " Este paciente ainda não foi monitorado"
                return render(request, 'supervisorio/error.html', {
                    'mensagem' : mensagem})
        else:
            # O id e o nome não correspondem ao mesmo paciente
            mensagem =  " O ID " + idpaciente + " nao corresponde ao (a) paciente " + nomepaciente
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})

def fc(request, nomepaciente, idpaciente):
    # idPaciente e nomePaciente não são valores que existem no banco?
    if not Paciente.objects.filter(nomePaciente=nomepaciente) and not Paciente.objects.filter(idPaciente=idpaciente):
        mensagem =  " Este paciente não existe"
        return render(request, 'supervisorio/error.html', {
            'mensagem' : mensagem})
    # Se ambos são valores existentes no banco..
    else:
        # Correspondem a mesma pessoa?
        if (check_pname_id(nomepaciente, idpaciente) == True):
            # Se correspondem a mesma pessoa, tem algum monitoramento?
            monitoramento = Monitoramento.objects.filter()
            if [ each.valorCardiaco for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]:
                # builds spo2 (y-axis) list
                param = [ each.valorCardiaco for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]
                # builds timestamp list
                #timestamp = []
                #for each in monitoramento:
                    #better later fix comparing id instead of names
                    #if (each.idPaciente.nomePaciente == nomepaciente):
                        #hour = str(each.timestamp.time().hour)
                        #minute = str(each.timestamp.time().minute)
                        #second = str(each.timestamp.time().second)
                        #time = hour + ":" + minute + ":" + second
                        #timestamp.append(time)

                return render(request, 'supervisorio/fc.html', {
                    'nomePaciente' : nomepaciente,
                    #'timestamp' : timestamp,
                    'id' : idpaciente,
                    'fc' : param})
            # Então não tem nenhum monitoramento?
            else:
                mensagem =  " Este paciente ainda não foi monitorado"
                return render(request, 'supervisorio/error.html', {
                    'mensagem' : mensagem})
        else:
            # O id e o nome não correspondem ao mesmo paciente
            mensagem =  " O ID " + idpaciente + " nao corresponde ao (a) paciente " + nomepaciente
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})

def fa(request, nomepaciente, idpaciente):
    # idPaciente e nomePaciente não são valores que existem no banco?
    if not Paciente.objects.filter(nomePaciente=nomepaciente) and not Paciente.objects.filter(idPaciente=idpaciente):
        mensagem =  " Este paciente não existe"
        return render(request, 'supervisorio/error.html', {
            'mensagem' : mensagem})
    # Se ambos são valores existentes no banco..
    else:
        # Correspondem a mesma pessoa?
        if (check_pname_id(nomepaciente, idpaciente) == True):
            # Se correspondem a mesma pessoa, tem algum monitoramento?
            monitoramento = Monitoramento.objects.filter()
            if [ each.fuga for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]:
                # builds spo2 (y-axis) list
                param = [ each.fuga for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]
                # builds timestamp list
                #timestamp = []
                #for each in monitoramento:
                    #better later fix comparing id instead of names
                    #if (each.idPaciente.nomePaciente == nomepaciente):
                        #hour = str(each.timestamp.time().hour)
                        #minute = str(each.timestamp.time().minute)
                        #second = str(each.timestamp.time().second)
                        #time = hour + ":" + minute + ":" + second
                        #timestamp.append(time)

                return render(request, 'supervisorio/fa.html', {
                    'nomePaciente' : nomepaciente,
                    #'timestamp' : timestamp,
                    'id' : idpaciente,
                    'fa' : param})
            # Então não tem nenhum monitoramento?
            else:
                mensagem =  " Este paciente ainda não foi monitorado"
                return render(request, 'supervisorio/error.html', {
                    'mensagem' : mensagem})
        else:
            # O id e o nome não correspondem ao mesmo paciente
            mensagem =  " O ID " + idpaciente + " nao corresponde ao (a) paciente " + nomepaciente
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})

def vt(request, nomepaciente, idpaciente):
    # idPaciente e nomePaciente não são valores que existem no banco?
    if not Paciente.objects.filter(nomePaciente=nomepaciente) and not Paciente.objects.filter(idPaciente=idpaciente):
        mensagem =  " Este paciente não existe"
        return render(request, 'supervisorio/error.html', {
            'mensagem' : mensagem})
    # Se ambos são valores existentes no banco..
    else:
        # Correspondem a mesma pessoa?
        if (check_pname_id(nomepaciente, idpaciente) == True):
            # Se correspondem a mesma pessoa, tem algum monitoramento?
            monitoramento = Monitoramento.objects.filter()
            if [ each.VCestimado for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]:
                # builds spo2 (y-axis) list
                param = [ each.VCestimado for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]
                # builds timestamp list
                #timestamp = []
                #for each in monitoramento:
                    #better later fix comparing id instead of names
                    #if (each.idPaciente.nomePaciente == nomepaciente):
                        #hour = str(each.timestamp.time().hour)
                        #minute = str(each.timestamp.time().minute)
                        #second = str(each.timestamp.time().second)
                        #time = hour + ":" + minute + ":" + second
                        #timestamp.append(time)

                return render(request, 'supervisorio/vt.html', {
                    'nomePaciente' : nomepaciente,
                    #'timestamp' : timestamp,
                    'id' : idpaciente,
                    'vt' : param})
            # Então não tem nenhum monitoramento?
            else:
                mensagem =  " Este paciente ainda não foi monitorado"
                return render(request, 'supervisorio/error.html', {
                    'mensagem' : mensagem})
        else:
            # O id e o nome não correspondem ao mesmo paciente
            mensagem =  " O ID " + idpaciente + " nao corresponde ao (a) paciente " + nomepaciente
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})

def allparams(request, nomepaciente, idpaciente):
    # idPaciente e nomePaciente não são valores que existem no banco?
    if not Paciente.objects.filter(nomePaciente=nomepaciente) and not Paciente.objects.filter(idPaciente=idpaciente):
        mensagem =  " Este paciente não existe"
        return render(request, 'supervisorio/error.html', {
            'mensagem' : mensagem})
    # Se ambos são valores existentes no banco..
    else:
        # Correspondem a mesma pessoa?
        if (check_pname_id(nomepaciente, idpaciente) == True):
            # Se correspondem a mesma pessoa, tem algum monitoramento?
            monitoramento = Monitoramento.objects.filter()
            if [ each.SpO2 for each in monitoramento if (each.idPaciente.idPaciente == int(idpaciente)) ]:
            # builds all parameters (y-axis) list
                spo2 = [ each.SpO2 for each in monitoramento if (int(each.idPaciente.idPaciente) == int(idpaciente)) ]
                fr = [ each.FRtotal for each in monitoramento if (int(each.idPaciente.idPaciente) == int(idpaciente)) ]
                fc = [ each.valorCardiaco for each in monitoramento if (int(each.idPaciente.idPaciente) == int(idpaciente)) ]
                fa = [ each.fuga for each in monitoramento if (int(each.idPaciente.idPaciente) == int(idpaciente)) ]
                vt = [ each.VCestimado for each in monitoramento if (int(each.idPaciente.idPaciente) == int(idpaciente)) ]
                # builds timestamp list
                #timestamp = []
                #for each in monitoramento:
                    #better later fix comparing id instead of names
                    #if (each.idPaciente.nomePaciente == nomepaciente):
                        #hour = str(each.timestamp.time().hour)
                        #minute = str(each.timestamp.time().minute)
                        #second = str(each.timestamp.time().second)
                        #time = hour + ":" + minute + ":" + second
                        #timestamp.append(time)

                return render(request, 'supervisorio/all.html', {
                    'nomePaciente' : nomepaciente,
                    'id' : idpaciente,
                    'spo2' : spo2,
                    'fr' : fr,
                    'fc' : fc,
                    'fa' : fa,
                    'vt' : vt})
            # Então não tem nenhum monitoramento?
            else:
                mensagem =  " Este paciente ainda não foi monitorado"
                return render(request, 'supervisorio/error.html', {
                    'mensagem' : mensagem})
        else:
            # O id e o nome não correspondem ao mesmo paciente
            mensagem =  " O ID " + idpaciente + " nao corresponde ao (a) paciente " + nomepaciente
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})

def add_patient(request):
    if request.method == 'POST':
        form = PacienteForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            form.save()
            mensagem =  " O paciente " + form.cleaned_data['nomePaciente'] + " foi cadastrado com sucesso"
            return render(request, 'supervisorio/error.html', {
                'mensagem' : mensagem})
    else:
        form = PacienteForm() # Unbound form
    return render(request, 'supervisorio/patient_add.html', {
        'form' : form,
    })
