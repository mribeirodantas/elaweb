# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import RegexValidator, MinLengthValidator
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.models.signals import post_save
from django.db.models.signals import pre_delete
import datetime


# Create your models here.
class Aparelho(models.Model):
    idAparelho = models.AutoField(primary_key=True, verbose_name="Número de identificação")
    dataLigacao = models.DateTimeField(auto_now_add=True, verbose_name="Quando foi ligado")
    modeloAparelho = models.CharField(max_length=20, verbose_name="Modelo",
                                      validators=[RegexValidator(regex='^[0-9 ]*[a-zA-Z ][a-zA-Z0-9 ]*$',
                                      message='O nome precisa conter ao menos uma letra, e sem espaços ou caracteres especiais.',
                                      code='nomatch'),
                                      MinLengthValidator(5)],)
    ativo = models.BooleanField(editable=False, default=False)
    nomeAparelho = models.CharField(max_length=20, verbose_name="Nome",
                                    validators=[RegexValidator(regex='^[0-9 ]*[a-zA-Z ][a-zA-Z0-9 ]*$',
                                    message='O nome precisa conter ao menos uma letra, e sem espaços ou caracteres especiais.',
                                    code='nomatch'),
                                    MinLengthValidator(5)],)

    def __unicode__(self):
        if self.ativo == True:
            usado = "Ocupado"
        else:
            usado = "Livre"

        return "%s" % (self.nomeAparelho)

    class Meta:
        app_label = u'Administração'
        db_table = 'supervisorio_aparelho'
        verbose_name = 'Aparelho'
        verbose_name_plural = 'Aparelhos'


class Paciente(models.Model):
    nomePaciente = models.CharField(max_length=40, verbose_name="Nome")
    cpfPaciente = models.CharField(max_length=14, verbose_name="CPF", unique=True,
                                   # Only digits
                                   validators=[RegexValidator(regex=r'^\d{3}\.\d{3}\.\d{3}\-\d{2}$',
                                   message='CPF inválido. Insira apenas números.',
                                   code='nomatch'),
                                   MinLengthValidator(14)],)
    susPaciente = models.CharField(max_length=18, verbose_name="Cartão SUS", unique=True,
                                   # Only digits
                                   validators=[RegexValidator(regex=r'^\d{3}\ \d{4}\ \d{4}\ \d{4}$',
                                   message='Número do cartão SUS inválido. Insira apenas números.',
                                   code='nomatch'),
                                   MinLengthValidator(18)],)
    sexoPaciente = models.CharField(max_length=1, verbose_name="Sexo")
    nomeCuidador = models.CharField(max_length=40, verbose_name="Cuidador")
    telefonePaciente = models.CharField(max_length=14, verbose_name="Telefone",
                                        # Only digits
                                        validators=[RegexValidator(regex=r'^\(\d{2}\)\ \d{4}-\d{4}$',
                                        message='Telefone inválido. Insira apenas números.',
                                        code='nomatch'),
                                        MinLengthValidator(14)],)
    pesoPaciente = models.FloatField(verbose_name="Peso")
    alturaPaciente = models.FloatField(verbose_name="Altura")
    nascimentoPaciente = models.DateField(verbose_name="Nascimento")
    enderecoPaciente = models.TextField(verbose_name="Endereço")
    cidadePaciente = models.CharField(max_length=30, verbose_name="Cidade")
    estadoPaciente = models.CharField(max_length=20, verbose_name="Estado")
    idPaciente = models.AutoField(primary_key=True, verbose_name="Número de identificação")
    cepPaciente = models.CharField(max_length=10, verbose_name="CEP",
                                   # Only digits
                                   validators=[RegexValidator(regex=r'\d{2}\.\d{3}\-\d{3}$',
                                   message='CEP inválido. Insira apenas números.',
                                   code='nomatch'),
                                   MinLengthValidator(10)],)
    aparelhoPaciente = models.ForeignKey('Aparelho', verbose_name="Aparelho", unique=True)

    def __unicode__(self):
        return "%s" % (self.nomePaciente)

    def clean(self):
        if not self.sexoPaciente in 'mfMF':
            raise ValidationError('Gênero não reconhecido.')
        # Modifying the patient
        try:
            try:
                # Modifying to a taken device
                if self.aparelhoPaciente.ativo == True:
                    if self.aparelhoPaciente != Paciente.objects.get(idPaciente=self.idPaciente).aparelhoPaciente:
                        raise ValidationError('Este aparelho já está em uso.')
                        # Updating new device and deactivating previous one
                    else:
                        paciente = Paciente.objects.get(idPaciente=self.idPaciente)
                        aparelho = Aparelho.objects.get(idAparelho=paciente.aparelhoPaciente.idAparelho, ativo=True)
                        self.aparelhoPaciente.ativo = True
                        self.aparelhoPaciente.save()
                        aparelho.ativo = False
                        aparelho.save()
            # Didn't select a device
            except Aparelho.DoesNotExist:
                raise ValidationError('Você precisa selecionar um aparelho.')

        # Adding a new patient
        except Paciente.DoesNotExist:
            try:
                self.aparelhoPaciente.ativo = self.aparelhoPaciente.ativo
                self.aparelhoPaciente.save()
            # Didn't select a device
            except Aparelho.DoesNotExist:
                raise ValidationError('Você precisa selecionar um aparelho.')
        self.aparelhoPaciente.ativo = False
        return self.clean_fields

    class Meta:
        app_label = u'Administração'
        db_table = 'supervisorio_paciente'
        verbose_name = 'Paciente'
        verbose_name_plural = 'Pacientes'
# It will active a device, as soon as you finish registering
# a patient associated with this device
def ativar_aparelho(sender, instance, **kwargs):
    instance.aparelhoPaciente.ativo = True
    instance.aparelhoPaciente.save()
# Registering the signal for when you register a new patient
post_save.connect(ativar_aparelho, sender=Paciente, dispatch_uid="activate_device2patient")
# It will deactive a device, as soon as you are about
# to delete a patient associated with this device
def desativar_aparelho(sender, instance, **kwargs):
    instance.aparelhoPaciente.ativo = False
    instance.aparelhoPaciente.save()
# Registering the signal for when you delete a patient
pre_delete.connect(desativar_aparelho, sender=Paciente, dispatch_uid="deactivate_device2patient")

# The primary key of this table will be id
# Monitoring consults will be done by idPaciente
# plus growing Id number.
class Monitoramento(models.Model):
    VCestimado = models.FloatField(verbose_name="VC estimado")
    fuga = models.IntegerField(verbose_name="Fuga de ar")
    FRtotal = models.IntegerField(verbose_name="Frequência Respiratória")
    SpO2 = models.IntegerField(verbose_name="Saturação de O2")
    # Remember that timestamp must be part of the compound primary key
    # But for now, Django can't handle composite primary key.
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name='Data')
    valorCardiaco = models.IntegerField(verbose_name="Pulso Cardíaco")
    idPaciente = models.ForeignKey('Paciente', verbose_name="Paciente")

    def __unicode__(self):
        return "%s (%s)" % (self.timestamp, self.idPaciente.nomePaciente)

    class Meta:
        app_label = u'Administração'
        db_table = 'supervisorio_monitoramento'
        verbose_name = 'Monitoramento'
        verbose_name_plural = 'Monitoramentos'

# alarmes gerados
class Alarmes(models.Model):
    tipoAlarme = models.IntegerField(verbose_name="Tipo de Alarme")
    timestsamp = models.DateTimeField(auto_now_add=False, verbose_name="Hora de disparo")
    idPaciente = models.ForeignKey('Paciente', verbose_name="Paciente")

    def __unicode__(self):
        return "%s (%s) [%s]" % (self.timestamp, tipoAlarme, self.idPaciente.nomePaciente)
    class Meta:
        app_label = u'Administração'
        db_table = 'supervisorio_alarmes'
        verbose_name = 'Alarmes'
