-- It sets a default now() value to timestamp
-- for when you're not using Django to update
-- the database.
ALTER TABLE supervisorio_monitoramento
    ALTER COLUMN timestamp SET DEFAULT now();
