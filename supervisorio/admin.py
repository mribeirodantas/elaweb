# -*- coding: utf-8 -*- 
from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from supervisorio.models import Aparelho, Paciente, Monitoramento


class PacienteAdmin(admin.ModelAdmin):
    #fields = ['nomePaciente', 'sexoPaciente', 'nomeCuidador', 'pesoPaciente', 'alturaPaciente', 'nascimentoPaciente',
    # 'enderecoPaciente', 'estadoPaciente', 'cidadePaciente', 'cepPaciente', 'aparelhoPaciente']
    fieldsets = [
            ('Dados pessoais', {'fields': ['nomePaciente', 'cpfPaciente',
                                           'susPaciente', 'sexoPaciente',
                                           'alturaPaciente', 'pesoPaciente',
                                           'nascimentoPaciente']}),
        ('Outros dados', {'fields': ['cidadePaciente', 'estadoPaciente',
                                     'enderecoPaciente', 'cepPaciente',
                                     'nomeCuidador', 'telefonePaciente',
                                     'aparelhoPaciente']})
            ]
    list_display = ('idPaciente', 'nomePaciente', 'aparelhoPaciente')



class AparelhoAdmin(admin.ModelAdmin):
    fieldsets = [
            ('Dados do Aparelho', {'fields': ['nomeAparelho', 'modeloAparelho']})]
    list_display = ('idAparelho', 'nomeAparelho', 'ativo', 'modeloAparelho', 'dataLigacao')
    actions = ['make_active', 'make_unactive']

    # We still need to figure out what happens if
    # there is a patient linked to that device
    def make_active(self, request, queryset):
        rows_updated = queryset.update(ativo=True)
        if rows_updated == 1:
            self.message_user(request, "1 aparelho foi ativado")
        else:
            self.message_user(request, "%s aparelhos foram ativados" % rows_updated)
    def make_unactive(self, request, queryset):
        rows_updated = queryset.update(ativo=False)
        if rows_updated == 1:
            self.message_user(request, "1 aparelho foi desativado")
        else:
            self.message_user(request, "%s aparelhos foram desativados" % rows_updated)
    #is it really short? lol
    make_active.short_description  = "Ativar os aparelhos selecionados"
    make_unactive.short_description = "Desativar os aparelhos selecionados"

class MonitoramentoAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'idPaciente', 'SpO2', 'VCestimado', 'fuga', 'FRtotal',
                    'valorCardiaco')
    list_filter = ['timestamp']
    date_hierarchy = 'timestamp'
    search_fields = ['idPaciente__nomePaciente']

    # Users should not be allowed to insert *fake* data
    # Only the microcontroller is allowed to write to this Model
    # Which means the only "fake" way is through psql
    # !!! Uncomment this for production !!!
    #def has_add_permission(self, request, obj=None):
    #    return False
    #def has_delete_permission(self, request, obj=None):
    #    return False

admin.site.register(Aparelho, AparelhoAdmin)
admin.site.register(Paciente, PacienteAdmin)
#admin.site.register(Monitoramento, MonitoramentoAdmin)
